#Build & start
FROM nginx:latest
RUN apt update && apt install -y git
RUN git clone https://gitlab.com/idzana/test-site
WORKDIR ./test-site
COPY ./index.html /usr/share/nginx/html/index.html
EXPOSE 80
RUN service nginx restart && service nginx status 
